
package sheridan;

/**
 *
 * @author akret
 */
public class DiscountByPercentage extends Discount {
    protected double discount;
    
    @Override
    public double calculateDiscount(double discountPercentage){
        return discount = amount * discountPercentage / 100;
    }
}
