
package sheridan;

/**
 *
 * @author akret
 */
public class DiscountFactory {
    private static DiscountFactory instance = null;
    
    private DiscountFactory(){}
    
    public Discount getDiscount(DiscountType type){
    switch ( type ) {
            case AMOUNT : return new DiscountByAmount( );
            case PERCENTAGE : return new DiscountByPercentage( );
        }
        return null;
    }
    
    public static DiscountFactory getInstance( ) {
        if ( instance == null ) {
            instance = new DiscountFactory( );
        }
        return instance;
    }
}

