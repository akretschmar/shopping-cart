
package sheridan;

/**
 *
 * @author akret
 */
public class DiscountByAmount extends Discount {
    protected double discount;
    
    @Override
    public double calculateDiscount(double discountDollarAmount){
        return discount = discountDollarAmount;
    }
}
