
package sheridan;

/**
 *
 * @author akret
 */
public enum DiscountType {
    AMOUNT, PERCENTAGE;
}
