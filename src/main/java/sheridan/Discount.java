
package sheridan;

/**
 *
 * @author akret
 */
public abstract class Discount {
        protected double amount;
                public abstract double calculateDiscount(double amount);
}
