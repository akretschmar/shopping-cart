
package sheridan;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author akret
 */
public class CartIT {
    
    public CartIT() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of setPaymentService method, of class Cart.
     */
//    @Test
//    public void testSetPaymentService() {
//        System.out.println("setPaymentService");
//        PaymentService service = null;
//        Cart instance = new Cart();
//        instance.setPaymentService(service);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of addProduct method, of class Cart.
     */
    @Test
    public void testAddProductGood() {
        System.out.println("isaddProductGood");
        Product product = null;
        Cart instance = new Cart();
        instance.addProduct(product);        
        
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(1,instance.getCartSize());
    }
    
    @Test
    public void testAddProductBad() {
        System.out.println("isaddProductBad");
        Product product = null;
        Cart instance = new Cart();
        instance.addProduct(product); 
        instance.addProduct(product);
        int expResult = 2;
        
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(2,instance.getCartSize());
    }

    /**
     * Test of payCart method, of class Cart.
     */
//    @Test
//    public void testPayCart() {
//        System.out.println("payCart");
//        Cart instance = new Cart();
//        instance.payCart();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getCartSize method, of class Cart.
//     */
//    @Test
//    public void testGetCartSize() {
//        System.out.println("getCartSize");
//        Cart instance = new Cart();
//        int expResult = 0;
//        int result = instance.getCartSize();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
